package resources

import (
	"net/http"

	"github.com/asaskevich/govalidator"
	"github.com/jinzhu/gorm"
	"github.com/labstack/echo"
	"github.com/labstack/gommon/log"

	"master-agen/models"

	"master-agen/repositories"
)

//CodeController for struct
type CodeController struct{}

//GetCode inserting
func GetCode(c echo.Context) error {
	db, ok := c.Get("db").(*gorm.DB)
	if !ok {
		return c.JSON(http.StatusNotFound, Error{
			Data: "Can't connect to database.",
		})
	}
	// Init codeRepo
	codeRepo := repositories.NewCodeRepo(db)

	// Init models
	code := new(models.Code)
	codes := codeRepo.FindAll(code)

	return c.JSON(http.StatusOK, StandardResponse{
		Data:   codes,
		Status: http.StatusOK,
	})
}

//GetCodeByID inserting
func GetCodeByID(c echo.Context) error {
	db, ok := c.Get("db").(*gorm.DB)
	if !ok {
		return c.JSON(http.StatusNotFound, Error{
			Data: "Can't connect to database.",
		})
	}

	// Init codeRepository
	codeRepo := repositories.NewCodeRepo(db)

	// Init models
	code := new(models.Code)
	firstCode := codeRepo.FindByID(code, c.Param("id"))

	return c.JSON(http.StatusOK, StandardResponse{
		Data:   firstCode,
		Status: http.StatusOK,
	})

}

//SaveCode inserting
func SaveCode(c echo.Context) error {
	db, ok := c.Get("db").(*gorm.DB)
	if !ok {
		return c.JSON(http.StatusNotFound, Error{
			Data: "Can't connect to database.",
		})
	}

	// Init codeRepository
	codeRepo := repositories.NewCodeRepo(db)

	// Init models
	code := new(models.Code)

	if err := c.Bind(code); err != nil {
		log.Errorf("error : %v", err)
	}

	//Validate User
	result, err := govalidator.ValidateStruct(code)
	if err != nil {
		v := govalidator.ErrorsByField(err)
		return c.JSON(http.StatusBadRequest, Error{
			Data: v,
		})
	}
	// Check if exists username only email
	exists := codeRepo.Find(&code, "reseller_code", code.ResellerCode)
	if exists {
		return c.JSON(http.StatusBadRequest, Error{
			Data: "Reseller is already assigned to code",
		})
	}
	save := codeRepo.Save(&code)
	if !save {
		return c.JSON(http.StatusInternalServerError, Error{
			Data: save,
		})
	}

	return c.JSON(http.StatusCreated, InsertResponse{
		Success: result,
	})
}

//UpdateCode inserting
func UpdateCode(c echo.Context) error {
	db, ok := c.Get("db").(*gorm.DB)
	if !ok {
		return c.JSON(http.StatusNotFound, Error{
			Data: "Can't connect to database.",
		})
	}

	// Init codeRepository
	codeRepo := repositories.NewAgentRepo(db)

	// Init models
	codeUpdate := new(models.CodeUpdate)

	if err := c.Bind(codeUpdate); err != nil {
		log.Errorf("error : %v", err)
	}

	//Validate User
	result, err := govalidator.ValidateStruct(codeUpdate)
	if err != nil {
		v := govalidator.ErrorsByField(err)
		return c.JSON(http.StatusBadRequest, Error{
			Data: v,
		})
	}
	save := codeRepo.Save(&codeUpdate)
	if !save {
		return c.JSON(http.StatusInternalServerError, Error{
			Data: save,
		})
	}

	return c.JSON(http.StatusCreated, InsertResponse{
		Success: result,
	})
}

//DeleteCode inserting
func DeleteCode(c echo.Context) error {
	db, ok := c.Get("db").(*gorm.DB)
	if !ok {
		return c.JSON(http.StatusNotFound, Error{
			Data: "Can't connect to database.",
		})
	}

	// Init codeRepository
	codeRepo := repositories.NewCodeRepo(db)

	// Init models
	codeDelete := new(models.CodeDelete)

	if err := c.Bind(codeDelete); err != nil {
		log.Errorf("error : %v", err)
	}

	//Validate User
	result, err := govalidator.ValidateStruct(codeDelete)
	if err != nil {
		v := govalidator.ErrorsByField(err)
		return c.JSON(http.StatusBadRequest, Error{
			Data: v,
		})
	}
	save := codeRepo.Save(&codeDelete)
	if !save {
		return c.JSON(http.StatusInternalServerError, Error{
			Data: save,
		})
	}

	return c.JSON(http.StatusCreated, InsertResponse{
		Success: result,
	})
}
