package resources

//Response base struct
type Response struct {
	Success bool        `json:"success"`
	Data    interface{} `json:"response"`
}

//InsertResponse base struct
type InsertResponse struct {
	Success bool `json:"success"`
}

//StandardResponse base struct
type StandardResponse struct {
	Status int         `json:"status"`
	Data   interface{} `json:"data"`
}

//Error base struct
type Error struct {
	Data interface{} `json:"error"`
}
