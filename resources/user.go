package resources

import (
	"net/http"

	"github.com/asaskevich/govalidator"
	"github.com/jinzhu/gorm"
	"github.com/labstack/echo"
	"github.com/labstack/gommon/log"

	"master-agen/helpers"

	"master-agen/models"

	"master-agen/repositories"
)

//UserController for struct
type UserController struct{}

//Register inserting
func (uc *UserController) Register(c echo.Context) error {
	db, ok := c.Get("db").(*gorm.DB)
	if !ok {
		return c.JSON(http.StatusNotFound, Error{
			Data: "Can't connect to database.",
		})
	}

	// Init UserRepository
	userRepo := repositories.NewUserRepo(db)

	// Init models
	user := new(models.User)
	userRegister := new(models.UserRegister)

	if err := c.Bind(userRegister); err != nil {
		log.Errorf("error : %v", err)
	}

	// Check if exists username only email
	exists := userRepo.Find(&user, "username", userRegister.Username)
	if exists {
		return c.JSON(http.StatusBadRequest, Error{
			Data: "Username already exists",
		})
	}

	//Validate User
	result, err := govalidator.ValidateStruct(userRegister)
	if err != nil {
		v := govalidator.ErrorsByField(err)
		return c.JSON(http.StatusBadRequest, Error{
			Data: v,
		})
	}

	//CheckPassword
	if userRegister.Password1 != userRegister.Password2 {
		return c.JSON(http.StatusBadRequest, Error{
			Data: "Passwords not match",
		})
	}
	user.Password = userRegister.Password1
	user.SetPassword()

	//Set original model and values for defaults
	user.Username = userRegister.Username
	user.RecoverToken = helpers.GenerateTokenRecovery()
	user.MasterAgentID = userRegister.MasterAgentID

	//need refactoring later
	user.IsActive = userRegister.IsActive
	user.IsDeleted = userRegister.IsDeleted
	user.CreatedBy = userRegister.CreatedBy
	save := userRepo.Save(&user)
	if !save {
		return c.JSON(http.StatusInternalServerError, Error{
			Data: "Contact to server. ",
		})
	}

	//Set userLogged for return
	userLogged := user.GenerateUserLogged()

	return c.JSON(http.StatusCreated, Response{
		Data:    userLogged,
		Success: result,
	})

}

//Login checker
func (uc *UserController) Login(c echo.Context) error {
	//Get DB
	db, ok := c.Get("db").(*gorm.DB)
	if !ok {
		return c.JSON(http.StatusNotFound, Error{
			Data: "Can't connect to database.",
		})
	}

	// Init UserRepository
	userRepo := repositories.NewUserRepo(db)
	user := new(models.User)

	//Bind form to Model.UserLogin
	if err := c.Bind(user); err != nil {
		log.Errorf("error: ", err)
		c.JSON(http.StatusInternalServerError, nil)
	}

	// Validate params
	if user.Username == "" || user.Password == "" {
		return c.JSON(http.StatusBadRequest, Error{
			Data: "Missing params",
		})
	}

	// Transform password
	user.SetPassword()

	success := userRepo.FindByCredentials(user)
	if !success {
		return c.JSON(http.StatusBadRequest, Error{
			Data: "Email or Password Invalid",
		})
	}

	userLogged := user.GenerateUserLogged()

	return c.JSON(http.StatusOK, Response{
		Data:    userLogged,
		Success: true,
	})

}
