package resources

import (
	"net/http"

	"github.com/asaskevich/govalidator"
	"github.com/jinzhu/gorm"
	"github.com/labstack/echo"
	"github.com/labstack/gommon/log"

	"master-agen/models"

	"master-agen/repositories"
)

//AgentController for struct
type AgentController struct{}

//GetAgent inserting
func GetAgent(c echo.Context) error {
	db, ok := c.Get("db").(*gorm.DB)
	if !ok {
		return c.JSON(http.StatusNotFound, Error{
			Data: "Can't connect to database.",
		})
	}

	// Init agentRepository
	agentRepo := repositories.NewAgentRepo(db)

	// Init models
	agent := new(models.Agent)
	agents := agentRepo.FindAll(agent)

	return c.JSON(http.StatusOK, StandardResponse{
		Data:   agents,
		Status: http.StatusOK,
	})
}

//GetAgentByID inserting
func GetAgentByID(c echo.Context) error {
	db, ok := c.Get("db").(*gorm.DB)
	if !ok {
		return c.JSON(http.StatusNotFound, Error{
			Data: "Can't connect to database.",
		})
	}

	// Init agentRepository
	agentRepo := repositories.NewAgentRepo(db)

	// Init models
	agent := new(models.Agent)
	firstAgent := agentRepo.FindByID(agent, c.Param("id"))

	return c.JSON(http.StatusOK, StandardResponse{
		Data:   firstAgent,
		Status: http.StatusOK,
	})

}

//SaveAgent inserting
func SaveAgent(c echo.Context) error {
	db, ok := c.Get("db").(*gorm.DB)
	if !ok {
		return c.JSON(http.StatusNotFound, Error{
			Data: "Can't connect to database.",
		})
	}

	// Init agentRepository
	agentRepo := repositories.NewAgentRepo(db)

	// Init models
	agent := new(models.Agent)

	if err := c.Bind(agent); err != nil {
		log.Errorf("error : %v", err)
	}

	//Validate User
	result, err := govalidator.ValidateStruct(agent)
	if err != nil {
		v := govalidator.ErrorsByField(err)
		return c.JSON(http.StatusBadRequest, Error{
			Data: v,
		})
	}
	// Check if exists username only email
	exists := agentRepo.Find(&agent, "email_address", agent.EmailAddress)
	if exists {
		return c.JSON(http.StatusBadRequest, Error{
			Data: "Email Address already exists",
		})
	}
	save := agentRepo.Save(&agent)
	if !save {
		return c.JSON(http.StatusInternalServerError, Error{
			Data: save,
		})
	}

	return c.JSON(http.StatusCreated, InsertResponse{
		Success: result,
	})
}

//UpdateAgent inserting
func UpdateAgent(c echo.Context) error {
	db, ok := c.Get("db").(*gorm.DB)
	if !ok {
		return c.JSON(http.StatusNotFound, Error{
			Data: "Can't connect to database.",
		})
	}

	// Init agentRepository
	agentRepo := repositories.NewAgentRepo(db)

	// Init models
	agentUpdate := new(models.AgentUpdate)

	if err := c.Bind(agentUpdate); err != nil {
		log.Errorf("error : %v", err)
	}

	//Validate User
	result, err := govalidator.ValidateStruct(agentUpdate)
	if err != nil {
		v := govalidator.ErrorsByField(err)
		return c.JSON(http.StatusBadRequest, Error{
			Data: v,
		})
	}
	// Check if exists username only email
	agentChecker := new(models.Agent)
	exists := agentRepo.FindByCredentials(agentUpdate, agentChecker)
	if exists {
		return c.JSON(http.StatusBadRequest, Error{
			Data: "Email Address already exists",
		})
	}
	save := agentRepo.Save(&agentUpdate)
	if !save {
		return c.JSON(http.StatusInternalServerError, Error{
			Data: save,
		})
	}

	return c.JSON(http.StatusCreated, InsertResponse{
		Success: result,
	})
}

//DeleteAgent inserting
func DeleteAgent(c echo.Context) error {
	db, ok := c.Get("db").(*gorm.DB)
	if !ok {
		return c.JSON(http.StatusNotFound, Error{
			Data: "Can't connect to database.",
		})
	}

	// Init agentRepository
	agentRepo := repositories.NewAgentRepo(db)

	// Init models
	agentDelete := new(models.AgentDelete)

	if err := c.Bind(agentDelete); err != nil {
		log.Errorf("error : %v", err)
	}

	//Validate User
	result, err := govalidator.ValidateStruct(agentDelete)
	if err != nil {
		v := govalidator.ErrorsByField(err)
		return c.JSON(http.StatusBadRequest, Error{
			Data: v,
		})
	}
	save := agentRepo.Save(&agentDelete)
	if !save {
		return c.JSON(http.StatusInternalServerError, Error{
			Data: save,
		})
	}

	return c.JSON(http.StatusCreated, InsertResponse{
		Success: result,
	})
}
