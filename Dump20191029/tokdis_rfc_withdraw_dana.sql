-- MySQL dump 10.13  Distrib 5.7.27, for Linux (x86_64)
--
-- Host: localhost    Database: tokdis_rfc
-- ------------------------------------------------------
-- Server version	5.7.27-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `withdraw_dana`
--

DROP TABLE IF EXISTS `withdraw_dana`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `withdraw_dana` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` bigint(20) DEFAULT NULL,
  `is_deleted` bit(1) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `receipt` varchar(255) DEFAULT NULL,
  `reseller_id` bigint(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `version` int(11) NOT NULL,
  `withdraw_funds` bigint(20) DEFAULT NULL,
  `destination_bank_id` bigint(20) DEFAULT NULL,
  `dompet_resellers_id` bigint(20) DEFAULT NULL,
  `withdraw_at` datetime DEFAULT NULL,
  `withdraw_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK9w4k609kgist44p5knynlcfoq` (`destination_bank_id`),
  KEY `FKcd00rgd7r8fq3wdk2esxld7jg` (`dompet_resellers_id`),
  CONSTRAINT `FK9w4k609kgist44p5knynlcfoq` FOREIGN KEY (`destination_bank_id`) REFERENCES `destination_bank` (`id`),
  CONSTRAINT `FKcd00rgd7r8fq3wdk2esxld7jg` FOREIGN KEY (`dompet_resellers_id`) REFERENCES `reseller` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `withdraw_dana`
--

LOCK TABLES `withdraw_dana` WRITE;
/*!40000 ALTER TABLE `withdraw_dana` DISABLE KEYS */;
/*!40000 ALTER TABLE `withdraw_dana` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-10-29 15:46:09
