package databases

import (
	"fmt"
	"log"

	"github.com/jinzhu/gorm"

	"master-agen/config"
)

//Init connect to mysql
func Init(conf *config.Config) *gorm.DB {

	connect := fmt.Sprintf("%s:%s@(%s:%s)/%s?parseTime=true",
		conf.Mysql.User,
		conf.Mysql.Password,
		conf.Mysql.Host,
		conf.Mysql.Port,
		conf.Mysql.DB,
	)

	db, err := gorm.Open("mysql", connect)
	if err != nil {
		log.Panicln(err)
	}

	return db
}
