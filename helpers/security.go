package helpers

import (
	"crypto/sha512"
	"encoding/hex"
	"time"
)

//GenerateTokenRecovery for lost id
func GenerateTokenRecovery() string {
	today := time.Now()
	k := today.String() + "m4st3r4g"
	h := sha512.New()
	h.Write([]byte(k))
	return hex.EncodeToString(h.Sum(nil))
}
