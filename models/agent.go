package models

//BaseAgent base struct
type BaseAgent struct {
	FullName     string `gorm:"type:varchar(255);not null" json:"full_name,omitempty" valid:"required"`
	EmailAddress string `gorm:"type:varchar(255);not null;" json:"email_address,omitempty" valid:"email,required"`
	Avatar       string `gorm:"type:varchar(255);not null" json:"avatar,omitempty" valid:"required"`
	Ktp          string `gorm:"type:varchar(255);not null" json:"ktp,omitempty" valid:"required"`
	Handphone    string `gorm:"type:varchar(100);not null" json:"handphone,omitempty" valid:"numeric,required"`
	DateOfBirth  string `gorm:"type:varchar(100);not null" json:"date_of_birth,omitempty" valid:"required"`
	Gender       string `gorm:"type:varchar(2);not null" json:"gender,omitempty" valid:"required"`
}

//Agent base struct
type Agent struct {
	BaseAgent
	Base
	Code []Code `gorm:"ForeignKey:MasterAgentID"`
}

//AgentUpdate struct
type AgentUpdate struct {
	BaseAgent
	BaseUpdate
}

//AgentDelete struct
type AgentDelete struct {
	BaseDelete
}

//TableName for return table name
func (Agent) TableName() string {
	return "master_agent"
}

//TableName for return table name
func (AgentUpdate) TableName() string {
	return "master_agent"
}

//TableName for return table name
func (AgentDelete) TableName() string {
	return "master_agent"
}
