package models

import "time"

//BaseCode base struct
type BaseCode struct {
	AssignedBy    int       `gorm:"column:assigned_by;type:int(11)" json:"assigned_by" valid:"required"`
	AssignedAt    time.Time `gorm:"column:assigned_at;type:datetime;default:null" json:"assigned_at"`
	RefCode       string    `gorm:"type:varchar(255);not null" json:"ref_code,omitempty" valid:"required"`
	ResellerCode  string    `gorm:"type:varchar(255);not null;" json:"reseller_code,omitempty" valid:"required"`
	ResellerID    uint      `gorm:"type:bigint(20)" json:"reseller_id" valid:"required"`
	SaldoVoucher  uint      `gorm:"type:bigint(20)" json:"saldo_voucher"`
	Token         string    `gorm:"type:varchar(255);" json:"token"`
	TotalMember   uint      `gorm:"type:bigint(20)" json:"total_member"`
	TotalSaldo    uint      `gorm:"type:bigint(20)" json:"total_saldo"`
	MasterAgentID uint      `gorm:"type:bigint(20)" json:"master_agent_id"`
}

//Code base struct
type Code struct {
	BaseCode
	ExistingBase
}

//CodeUpdate struct
type CodeUpdate struct {
	BaseCode
	BaseUpdate
}

//CodeDelete struct
type CodeDelete struct {
	ExistingBaseDelete
}

//TableName for return table name
func (Code) TableName() string {
	return "codes"
}

//TableName for return table name
func (CodeUpdate) TableName() string {
	return "codes"
}

//TableName for return table name
func (CodeDelete) TableName() string {
	return "codes"
}
