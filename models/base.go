package models

import "time"

//Base struct
type Base struct {
	ID        uint      `gorm:"primary_key;type:int(11), AUTO_INCREMENT"`
	IsActive  bool      `gorm:"column:is_active;type:int(1)" json:"is_active"`
	IsDeleted bool      `gorm:"column:is_deleted;type:int(1)" json:"is_deleted"`
	CreatedBy int       `gorm:"column:created_by;type:int(11)" json:"created_by" valid:"required"`
	CreatedAt time.Time `gorm:"column:created_at;type:datetime;default:null" json:"created_at"`
}

//ExistingBase struct
type ExistingBase struct {
	ID        uint      `gorm:"primary_key;type:int(11), AUTO_INCREMENT"`
	CreatedBy int       `gorm:"column:created_by;type:int(11)" json:"created_by" valid:"required"`
	CreatedAt time.Time `gorm:"column:created_at;type:datetime;default:null" json:"created_at"`
}

//BaseUpdate struct
type BaseUpdate struct {
	ID        uint      `gorm:"primary_key;type:int(11)" json:"id" valid:"required"`
	UpdatedBy int       `gorm:"column:updated_by;type:int(11);default:1" json:"updated_by"`
	UpdatedAt time.Time `gorm:"column:updated_at;type:datetime;default:now()" json:"updated_at"`
}

//BaseDelete struct
type BaseDelete struct {
	ID        uint      `gorm:"primary_key;type:int(11)" json:"id" valid:"required"`
	IsActive  bool      `gorm:"column:is_active;type:int(1)" json:"is_active"`
	IsDeleted bool      `gorm:"column:is_deleted;type:int(1)" json:"is_deleted"`
	DeletedBy int       `gorm:"column:updated_by;type:int(11);default:1" json:"deleted_by"`
	DeletedAt time.Time `gorm:"column:updated_at;type:datetime;default:now()" json:"deleted_at"`
}

//ExistingBaseDelete struct
type ExistingBaseDelete struct {
	ID        uint      `gorm:"primary_key;type:int(11)" json:"id" valid:"required"`
	DeletedBy int       `gorm:"column:updated_by;type:int(11);default:1" json:"deleted_by"`
	DeletedAt time.Time `gorm:"column:updated_at;type:datetime;default:now()" json:"deleted_at"`
}
