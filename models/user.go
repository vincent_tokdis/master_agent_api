package models

import (
	"encoding/hex"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/gommon/log"
	"golang.org/x/crypto/argon2"

	"master-agen/config"
)

//BaseUser base struct
type BaseUser struct {
	Username      string `gorm:"type:varchar(100);not null" json:"username,omitempty" valid:"required"`
	Password      string `gorm:"type:varchar(255);not null" json:"password,omitempty" valid:"required"`
	MasterAgentID int    `gorm:"column:master_agent_id;type:varchar(255);not null" json:"master_agent_id,omitempty" valid:"required"`
	RecoverToken  string `gorm:"type:varchar(255);not null" json:"recover_token,omitempty" valid:"required"`
}

//User base struct
type User struct {
	BaseUser
	Base
}

//UserUpdate struct
type UserUpdate struct {
	BaseUser
	BaseUpdate
}

//UserDelete struct
type UserDelete struct {
	BaseDelete
}

//UserRegister base struct
type UserRegister struct {
	Base
	Username      string `json:"username" valid:"required"`
	Password1     string `json:"password1" valid:"required"`
	Password2     string `json:"password2" valid:"required"`
	MasterAgentID int    `gorm:"type:varchar(255);not null" json:"master_agent_id,omitempty" valid:"required"`
}

//UserLogged base struct
type UserLogged struct {
	MasterAgentID int    `json:"master_agent_id"`
	Username      string `json:"username,omitempty"`
	Jwt           string
}

// JwtCustomClaims are custom claims extending default ones.
type JwtCustomClaims struct {
	Username string `json:"username"`
	jwt.StandardClaims
}

//TableName for return table name
func (User) TableName() string {
	return "master_agent_user"
}

//TableName for return table name
func (UserUpdate) TableName() string {
	return "master_agent_user"
}

//TableName for return table name
func (UserDelete) TableName() string {
	return "master_agent_user"
}

//SetPassword for hash password
func (u *User) SetPassword() {
	cfg := config.GetConfig()
	key := argon2.Key([]byte(u.Password), cfg.Server.PasswordSalt, 3, 32*1024, 4, 32)
	u.Password = hex.EncodeToString(key)
}

//generateUserJwt for generate jwt when login
func (u *User) generateUserJwt(origin *UserLogged) (error, string) {
	// cfg := config.GetConfig()
	token := jwt.New(jwt.SigningMethodHS256)

	// Set claims
	claims := token.Claims.(jwt.MapClaims)
	claims["name"] = origin.Username
	claims["login"] = true
	claims["exp"] = time.Now().Add(time.Hour * 1).Unix()

	// Generate encoded token and send it as response.
	t, err := token.SignedString([]byte("m4st3r4g"))
	if err != nil {
		return err, ""
	}

	return nil, t
}

//GenerateUserLogged for userlogged
func (u *User) GenerateUserLogged() *UserLogged {

	userLog := new(UserLogged)
	userLog.MasterAgentID = u.MasterAgentID
	userLog.Username = u.Username

	//Generate JWT
	err, jwt := u.generateUserJwt(userLog)
	if err != nil {
		log.Errorf("error: %v", err)
		return nil
	}

	// Set JWT
	userLog.Jwt = jwt

	return userLog
}
