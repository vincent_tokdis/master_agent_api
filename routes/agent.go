package routes

import (
	"master-agen/resources"

	"github.com/labstack/echo"
)

//RegisterAgent for call the controller
func RegisterAgent(e *echo.Echo) {

	e.GET("agent", resources.GetAgent)
	e.POST("agent", resources.SaveAgent)
}
