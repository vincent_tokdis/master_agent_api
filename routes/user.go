package routes

import (
	"github.com/labstack/echo"

	"master-agen/resources"
)

//RegisterUser for call the controller
func RegisterUser(e *echo.Echo) {
	userController := new(resources.UserController)

	e.POST("auth/register", userController.Register)
	e.POST("auth/login", userController.Login)
}
