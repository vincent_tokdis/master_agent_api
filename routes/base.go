package routes

import (
	"master-agen/resources"

	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

func isBaseLogin(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		user := c.Get("user").(*jwt.Token)
		claims := user.Claims.(jwt.MapClaims)
		isAdmin := claims["login"].(bool)
		if isAdmin == false {
			return echo.ErrUnauthorized
		}
		return next(c)
	}
}

//IsBaseLoggedIn for check JWT parse key
var IsBaseLoggedIn = middleware.JWTWithConfig(middleware.JWTConfig{
	SigningKey: []byte("m4st3r4g"),
})

//Init the route
func Init(e *echo.Echo) {
	RegisterUser(e)
	e.GET("agent", resources.GetAgent, IsBaseLoggedIn, isBaseLogin)
	e.GET("agentperid/:id", resources.GetAgentByID, IsBaseLoggedIn, isBaseLogin)
	e.POST("agent", resources.SaveAgent, IsBaseLoggedIn, isBaseLogin)
	e.PUT("agent/:id", resources.UpdateAgent, IsBaseLoggedIn, isBaseLogin)
	//for codes
	e.GET("code", resources.GetCode)
}
