package repositories

import (
	"master-agen/models"

	"github.com/jinzhu/gorm"
)

//AgentRepository base struct
type AgentRepository struct {
	*Repository
}

//NewAgentRepo base struct
func NewAgentRepo(db *gorm.DB) *AgentRepository {
	return &AgentRepository{&Repository{db}}
}

//FindAll for calling method
func (ur *AgentRepository) FindAll(agent *models.Agent) []models.Agent {
	var agents []models.Agent
	ur.DB.Preload("Code").Find(&agents)
	return agents
}

//FindByID for calling method
func (ur *AgentRepository) FindByID(agent *models.Agent, paramID string) models.Agent {
	var firstAgent models.Agent

	ur.DB.First(&firstAgent, paramID)
	return firstAgent
}

//FindByCredentials for checking
func (ur *AgentRepository) FindByCredentials(agentUpdate *models.AgentUpdate, agent *models.Agent) bool {
	if ur.DB.Where("email_address = ? AND id != ?", agentUpdate.EmailAddress, agentUpdate.ID).First(&agent).RecordNotFound() {
		return false
	}

	return true
}
