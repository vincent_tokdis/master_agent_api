package repositories

import (
	"master-agen/models"

	"github.com/jinzhu/gorm"
)

//CodeRepository base struct
type CodeRepository struct {
	*Repository
}

//NewCodeRepo base struct
func NewCodeRepo(db *gorm.DB) *CodeRepository {
	return &CodeRepository{&Repository{db}}
}

//FindAll for calling method
func (ur *CodeRepository) FindAll(code *models.Code) []models.Code {
	var codes []models.Code

	ur.DB.Find(&codes)
	return codes
}

//FindByID for calling method
func (ur *CodeRepository) FindByID(code *models.Code, paramID string) models.Code {
	var firstCode models.Code

	ur.DB.First(&firstCode, paramID)
	return firstCode
}
