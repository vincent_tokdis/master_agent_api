package repositories

import (
	"github.com/jinzhu/gorm"

	"master-agen/models"
)

//UserRepository base struct
type UserRepository struct {
	*Repository
}

//NewUserRepo base struct
func NewUserRepo(db *gorm.DB) *UserRepository {
	return &UserRepository{&Repository{db}}
}

//FindByCredentials for calling method
func (ur *UserRepository) FindByCredentials(user *models.User) bool {
	if ur.DB.Where("username = ? and password = ? and is_active = 1 and is_deleted = 0", user.Username, user.Password).First(&user).RecordNotFound() {
		return false
	}

	return true
}
